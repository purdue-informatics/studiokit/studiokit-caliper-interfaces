﻿# StudioKit.Caliper.Interfaces

Defines interfaces for working with the **StudioKit.Caliper** library.

* ICaliperConfiguration
* ICaliperConfigurationProvider